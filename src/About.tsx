import React from 'react';

const About: React.FC = () => {
    return (
        <div>
            <h2>About</h2>
            <p>Dies ist die About-Seite.</p>
        </div>
    );
}

export default About;
